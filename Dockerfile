FROM docker:dind

RUN apk update
RUN apk add python3 py-pip libffi-dev openssl-dev gcc libc-dev make curl docker-compose
RUN pip install awscli